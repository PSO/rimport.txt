"""
Formatage des données pour un export Quadra.
"""
import backends.quadra_codec


class LigneQuadra(object):

    def __init__(self, ligne: 'Ligne'):
        self.ligne = ligne
        self.compte = self.ligne.compte
        if not self.compte[0].isnumeric():
            self.compte = "9" + self.compte

    @staticmethod
    def text_field(txt, width, errors='strict'):
        if txt is None:
            return b' ' * width
        elif type(txt) is bytes:
            return txt.ljust(width)[:width]
        else:
            return txt.ljust(width)[:width].encode(encoding='Quadra', errors=errors)

    @staticmethod
    def num_field(num, width, errors='strict'):
        if num is None:
            return b' ' * width
        else:
            return str(num).rjust(width)[:width].encode(encoding='ascii', errors=errors)

    def date_field(self):
        return self.ligne.date.strftime("%d%m%y").encode(encoding='ascii')

    def dc_field(self):
        if self.ligne.montant < 0:
            return b'C'
        else:
            return b'D'

    def amount_field(self):
        if self.ligne.montant < 0:
            amount = -self.ligne.montant
        else:
            amount = self.ligne.montant
        return "+{:0=12}".format(int(amount * 100)).encode(encoding='ascii')

    def data(self, ident=None) -> bytes:
        """
        Presque fidele à :
        https://help.eurecia.com/hc/fr/articles/360000581669-Format-CEGID-QUADRATUS-COMPTABILITE
        """
        return b''.join([
            b'M',
            self.text_field(self.compte, 8),
            b'VT000',
            self.date_field(),
            b' ',
            self.text_field(self.ligne.titre, 20, errors='replace'),
            self.dc_field(),
            self.amount_field(),
            b' ' * 52,
            b'EUR',
            b' ' * 6,
            self.text_field(self.ligne.titre, 29, errors='replace'),
            self.num_field(ident, 10),
            b'\0',
            b' ' * 100,
            b'\r\n'
        ])
