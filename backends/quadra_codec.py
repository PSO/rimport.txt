"""
Adapted from https://stackoverflow.com/questions/38777818/how-do-i-properly-create-custom-text-codecs
"""

import codecs

from typing import Tuple

# prepare map from numbers to letters
_encode_table = {
    'Ç': b'\200',
    'ü': b'\201',
    'é': b'\202',
    'â': b'\203',
    'ä': b'\204',
    'à': b'\205',
    'å': b'\206',
    'ç': b'\207',
    'ê': b'\x88',
    'ë': b'\x89',
    'è': b'\x8A',
    'ï': b'\x8B',
    'î': b'\x8C',
    'ì': b'\x8D',
    'Ä': b'\x8E',
    'Å': b'\x8F',
    'É': b'\x90',
    'æ': b'\x91',
    'Æ': b'\x92',
    # 'Æ': b'\x93',
    'ö': b'\x94',
    'ò': b'\x95',
    'ô': b'\x6F',  # Special Lô-Anh
    'û': b'\x96',
    'ù': b'\x97',
    'ú': b'\x97',  # Special Lúa Kyriakou
    'ÿ': b'\x98',
    'Ö': b'\x99',
    'Ü': b'\x9A'
}


def quadra_encode_char(c: str, errors):
    if c in _encode_table:
        return _encode_table[c]
    else:
        return c.encode('ascii', errors=errors)


def quadra_encode(text: str, errors='strict') -> Tuple[bytes, int]:
    # see https://docs.python.org/3/library/codecs.html#codecs.Codec.encode
    return b''.join(quadra_encode_char(x, errors) for x in text), len(text)


def quadra_decode(binary: bytes, errors='strict') -> Tuple[str, int]:
    # see https://docs.python.org/3/library/codecs.html#codecs.Codec.decode
    raise NotImplementedError


def quadra_search_function(encoding_name):
    return codecs.CodecInfo(quadra_encode, quadra_decode, name='Quadra')


# register your custom codec
# note that CodecInfo.name is used later
codecs.register(quadra_search_function)


def main():
    text = "Adhésion à l'association mère"
    print(text)
    # encode numbers to letters
    binary = text.encode(encoding='Quadra', errors='replace')
    print(binary)


if __name__ == '__main__':
    main()
