from decimal import Decimal

from backends.quadra import LigneQuadra


class Ligne(object):
    def __init__(self, date, compte, montant: Decimal, titre):
        self.date = date
        self.compte = str(compte)
        self.montant = montant
        self.titre = titre  # Used to be str(titre)

    def __iadd__(self, other):
        self.montant += other
        return self

    def __isub__(self, other):
        self.montant -= other
        return self

    def dump_ciel(self, out):
        def q(data):
            return '"%s"' % str(data)

        date = self.date.strftime("%d/%m/%Y")
        if self.montant < 0:
            montant, dc = -self.montant, 'C'
        else:
            montant, dc = self.montant, 'D'
        montant = ("%.2f" % montant).replace('.', ',')
        compte = self.compte
        if not compte[0].isnumeric():
            compte = "411" + self.compte
        print(q(0), q('VT'), q(date), q(compte), '', q(montant), dc, 'B', q(self.titre),
              sep='\t', end='\r\n', file=out)

    def dump_quadra(self, out, ident):
        data = LigneQuadra(self).data(ident=ident)
        out.write(data)


class Ecriture(object):
    def __init__(self, options):
        self.solde = Ligne(options.date, options.compte, Decimal('0.00'), options.titre)
        self.lines = []

    def add_line(self, line):
        self.lines.append(line)
        self.solde -= line.montant

    def dump_ciel(self, out):
        print("##Transfert", file=out, end='\r\n')
        print("##Section\tMvt", file=out, end='\r\n')
        self.solde.dump_ciel(out)
        for line in self.lines:
            if line.montant:
                line.dump_ciel(out)

    def dump_quadra(self, out, first_ident=None):
        ident = first_ident
        self.solde.dump_quadra(out, ident)
        if ident is not None:
            ident += 1
        for line in self.lines:
            line.dump_quadra(out, ident)
            if ident is not None:
                ident += 1
