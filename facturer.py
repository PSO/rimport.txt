#!/usr/bin/python3
"""
Générateur de fichier d'import CIEL.

Usage:
  __file__ [options] <fichier_csv> [-o <fichier_txt>]

Options générales:
--titre=TXT    Intitulé du mouvement
--date=DATE    Date du mouvement
--compte=NUM   Compte CIEL utilisé pour le solde

Options facultatives:
--mois=NOM     Nom du mois
"""
from decimal import Decimal

import chardet
from docopt import docopt
from datetime import datetime
from pathlib import Path
import re
import csv
import sys

import shorthands
from compta import Ecriture, Ligne
from mois import Mois, Ambiguous


def file_indicator(filename, ext):
    p = re.compile('^.*-(.*)\\.%s$' % ext)
    m = p.match(str(filename))
    if m:
        return m.group(1)
    else:
        raise ValueError("No %s indicator in: %s" % (ext, filename))


def month_indicator(filename, ext):
    indicator = file_indicator(filename, ext)
    return Mois.nom_vers_mois(indicator)


class Options(object):
    def __init__(self, arguments):
        def required(item):
            if arguments[item] is None:
                raise ValueError(item + " requis")
            return arguments[item]

        self.csv = required('<fichier_csv>')
        self.titre = required('--titre')
        self.date = required('--date')
        self.compte = required('--compte')

        self.mois = arguments['--mois']
        self.txt = arguments['<fichier_txt>']


class OptionsResolved(object):

    def __init__(self, opt):
        def set_mois(source, mois):
            if self.mois is None:
                self.mois = mois
                self.source = source
            else:
                if mois != self.mois:
                    print("Attention: conflit entre %s et l'option %s" % (str(self.mois.nom_long()),
                                                                          source), file=sys.stderr)

        self.mois = None
        if opt.mois:
            set_mois('--mois', Mois.nom_vers_mois(opt.mois))
        try:
            self.date = None
            self.date = datetime.strptime(opt.date, "%d/%m/%Y")
            set_mois('--date', Mois(self.date))
        except ValueError:
            pass
        if opt.txt:
            try:
                set_mois('-o', month_indicator(opt.txt, 'txt'))
            except ValueError:
                pass
            except Ambiguous as exn:
                if not self.mois:
                    print("Attention: %s" % str(exn), file=sys.stderr)
        try:
            set_mois('<fichier_csv>', month_indicator(opt.csv, 'csv'))
        except ValueError:
            pass
        except Ambiguous as exn:
            if not self.mois:
                print("Attention: %s" % str(exn), file=sys.stderr)
        if self.mois is None and self.date is None:
            raise ValueError("Impossible de déterminer le mois")

        if self.date is None:
            if opt.date == "%D":
                self.date = self.mois.debut()
            elif opt.date == "%F":
                self.date = self.mois.fin()
            else:
                raise ValueError("Impossible de déterminer la date")

        self.titre = opt.titre
        self.titre = self.titre.replace('%m', self.mois.nom_court())
        self.titre = self.titre.replace('%2m', '%s-%s' % (self.mois.nom_court(), self.mois.suivant().nom_court()))

        csv_file = opt.csv
        csv_file = csv_file.replace('%m', self.mois.nom_systeme())
        self.csv = Path(csv_file)

        if opt.txt:
            txt_file = opt.txt
            txt_file = txt_file.replace('%m', self.mois.nom_systeme())
            self.txt = Path(txt_file)
        else:
            self.txt = self.csv.with_suffix('.txt')

        self.compte = opt.compte

    def __str__(self):
        return "--titre='%s' --date=%s --compte=%s '%s' -o '%s'" % (self.titre, self.date.strftime("%d/%m/%Y"),
                                                                    self.compte, self.csv, self.txt)


class CSVLine(object):

    def __init__(self, compte, montant, precision):
        self.compte = compte
        self.montant = montant
        self.precision = precision

    def __repr__(self):
        return f'{self.__class__.__name__}({self.compte}, {self.montant}, {self.precision})'

    def to_ligne(self, date, titre):
        if self.precision:
            titre += " " + self.precision
        return Ligne(date, self.compte, self.montant, titre)


def load_csv_lines(data):
    # Developped for Appli
    lines = []
    data = iter(csv.reader(data))
    next(data)  # skip the header
    for row in data:
        compte = row[0]
        montant = row[1]
        if not montant:
            continue
        montant = Decimal(montant.replace(',', '.').replace('€', ''))
        if montant == 0:
            continue
        if len(row) >= 3:
            precision = row[2]
        else:
            precision = None
        lines.append(CSVLine(compte, montant, precision))
    return lines


def ecriture_of_lines(lines, options):
    # Developped for Appli
    ecriture = Ecriture(options)
    for line in lines:
        ecriture.add_line(line.to_ligne(options.date, options.titre))
    return ecriture


def load_csv(data, options):
    # Detecting the delimiter
    dialect = csv.Sniffer().sniff(data.read(1024), delimiters=',;')
    data.seek(0)

    # Building the corresponding Ecriture
    ecriture = Ecriture(options)
    data = iter(csv.reader(data, dialect))
    next(data)  # skip the header
    for row in data:
        compte = row[0]
        montant = row[1]
        if not montant:
            continue
        montant = montant.replace(',', '.').replace('€', '')
        try:
            montant = Decimal(montant)
        except Exception as e:
            print(f'Erreur: Montant étrange: {repr(montant)}')
            raise e
        if montant == 0:
            continue
        titre = options.titre
        if len(row) >= 3:
            precision = row[2]
            if precision:
                titre += ' ' + precision
        ecriture.add_line(Ligne(options.date, compte, montant, titre))
    return ecriture


def process_csv_file(csv_file, out_file, options, ciel=False):
    # Detecting the encoding
    with csv_file.open('rb') as raw_data:
        charset = chardet.detect(raw_data.read(1024))

    with csv_file.open('rt', encoding=charset['encoding']) as data:
        ecriture = load_csv(data, options)
        if ciel:
            with out_file.open('wt', encoding='iso-8859-1') as out:
                ecriture.dump_ciel(out)
        else:
            with out_file.open('wb') as out:
                ecriture.dump_quadra(out, first_ident=None)


def process_csv_lines(lines, out_file, options, ciel=False):
    # TODO: support variable encoding
    # Developed for Appli
    ecriture = ecriture_of_lines(lines, options)
    if ciel:
        with out_file.open('wt', encoding='iso-8859-1') as out:
            ecriture.dump_ciel(out)
    else:
        with out_file.open('wb') as out:
            ecriture.dump_quadra(out, first_ident=None)


def main():
    command_args = sys.argv[1:]
    expanded_args = shorthands.expand(command_args)
    # parsed_args = docopt(__doc__.replace('__file__', __file__) + shorthands.doc, expanded_args)
    doc = __doc__.replace('__file__', __file__)
    parsed_args = docopt(doc + shorthands.doc, expanded_args)
    options = Options(parsed_args)
    options = OptionsResolved(options)
    print("%s %s" % (__file__, str(options)), file=sys.stderr)
    process_csv_file(options.csv, options.txt, options)

    # TODO: faire que la dernière valeur d'une option écrase les précédentes


if __name__ == '__main__':
    main()
