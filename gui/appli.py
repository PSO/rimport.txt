import tkinter as tk
from datetime import date
from pathlib import Path
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror, showinfo

import docopt
from tkcalendar import DateEntry

import facturer
from facturer import load_csv_lines, process_csv_lines
from mois import Mois
from shorthands import shorthand_dict


class MyWindow(tk.Tk):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.title('Quadra Converter')
        self.geometry('800x600')
        self.resizable(False, False)

        # frame
        frame = ttk.Frame(self)

        # field options
        options = {'padx': 5, 'pady': 5}

        # CSV label
        csv_label = ttk.Label(frame, text='Fichier CSV')
        csv_label.grid(column=0, row=0, sticky='W', **options)

        # csv_entry.focus()

        # convert button

        csv_button = ttk.Button(frame, text='Choisir...')
        csv_button.grid(column=1, row=0, sticky='W', **options)
        csv_button.configure(command=lambda: self.csv_button_clicked())

        self.current_dir = None
        self.current_file = None
        self.lines = None

        # result label
        self.result_label = ttk.Label(frame, justify=tk.RIGHT)
        self.result_label.grid(row=0, column=2, **options)

        self.shorthand = tk.StringVar(frame, '')
        shorthand_frame = ttk.Frame(frame)
        shorthand_frame.grid(row=1, columnspan=3, **options)
        col_num = 6
        for i, short in enumerate(shorthand_dict.keys()):
            radio = ttk.Radiobutton(shorthand_frame, text=short[2:],
                                    value=short,
                                    variable=self.shorthand,
                                    command=lambda: self.change_option())
            radio.grid(column=i % col_num, row=i // col_num, sticky='W', **options)

        self.expanded_text = tk.StringVar(frame, '')
        self.expanded = ttk.Entry(frame, textvariable=self.expanded_text, width=80)
        self.expanded.grid(row=2, columnspan=3, **options)

        # DATE
        date_label = ttk.Label(frame, text="Date :")
        date_label.grid(row=4, column=0, **options)
        today = date.today()
        self.date = DateEntry(frame, width=30, day=1, month=today.month, year=today.year)
        self.date.grid(row=4, column=1, columnspan=2, **options)

        # ACCOUNT
        account_label = ttk.Label(frame, text="Compte :")
        account_label.grid(row=5, column=0, **options)
        self.account_text = tk.StringVar()
        account_entry = ttk.Entry(frame, textvariable=self.account_text, width=60)
        account_entry.grid(row=5, column=1, columnspan=2, **options)

        # TITLE
        title_label = ttk.Label(frame, text="Titre :", justify=tk.LEFT)
        title_label.grid(row=6, column=0, **options)
        self.title_text = tk.StringVar()
        title_entry = ttk.Entry(frame, textvariable=self.title_text, width=60)
        title_entry.grid(row=6, column=1, columnspan=2, **options)

        # OUT
        title_label = ttk.Label(frame, text="Sortie :", justify=tk.LEFT)
        title_label.grid(row=7, column=0, **options)
        self.out_text = tk.StringVar()
        out_entry = ttk.Entry(frame, textvariable=self.out_text, width=80)
        out_entry.grid(column=1, columnspan=2, row=7, **options)

        generate_button = ttk.Button(frame, text='Générer Quadra',
                                     command=lambda: self.generate_quadra())
        generate_button.grid(row=8, column=2)

        # add padding to the frame and show it
        frame.grid(padx=10, pady=10)

    def csv_button_clicked(self):
        """  Handle convert button click event
        """
        file_types = [('Fichiers CSV', '.csv'),
                      ('Tous fichiers', '*')]
        csv_filename = askopenfilename(filetypes=file_types,
                                       initialdir=None)
        if csv_filename:
            # TODO: popup in case of failure
            # TODO: display summary with encoding + sep (factorized with command line)
            csv_filename = Path(csv_filename)
            with csv_filename.open('rt', encoding='utf-8') as data:
                self.lines = load_csv_lines(data)
                self.current_file = csv_filename.name
                text = f'{self.current_file} ({len(self.lines)} lignes)'
                self.result_label.config(text=text)
                out_file = csv_filename.with_suffix('.txt')
                self.out_text.set(out_file)

    def change_option(self):
        txt_options = shorthand_dict.get(self.shorthand.get())
        expanded = " ".join(txt_options)
        self.expanded_text.set(expanded)
        try:
            mois = Mois(self.date.get_date())
            txt_options.append(f'--mois={mois.nom_long()}')
            if self.out_text.get().endswith('.csv'):
                txt_options.append(self.out_text.get())
            else:
                txt_options.append('dummy.csv')
            docopt_options = docopt.docopt(facturer.__doc__, txt_options)
            options = facturer.Options(docopt_options)
            options_res = facturer.OptionsResolved(options)
            self.date.set_date(options_res.date)
            self.account_text.set(options_res.compte)
            self.title_text.set(options_res.titre)
        except Exception as e:
            raise e


    def generate_quadra(self):
        title = self.title_text.get()
        try:
            options = AppliOptions(self.date.get_date(), title, self.account_text.get())
            out_file = Path(self.out_text.get())
            process_csv_lines(self.lines, out_file, options)
        except Exception as e:
            showerror(title='Erreur de conversion Quadra', message=str(e))
            raise e
        showinfo(title='Conversion réussie', message=f'{title} exporté avec succès')


class AppliOptions(object):

    def __init__(self, date, titre, compte):
        self.date = date
        if not titre:
            raise ValueError('Titre requis')
        self.titre = titre
        if not compte:
            raise ValueError('Compte requis')
        self.compte = compte


# start the app
root = MyWindow()
root.mainloop()
