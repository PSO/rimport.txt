"""
Les mois en français.
"""

from babel.dates import format_date

import datetime
import unicodedata


def lower_ascii(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore')
    lower = only_ascii.lower().decode('utf-8')
    return lower


class Ambiguous(Exception):
    pass


class Mois(object):
    def __init__(self, *args):
        if len(args) == 0:
            self.ref = datetime.date.today()
        elif len(args) == 1:
            if type(args[0] is datetime.date):
                self.ref = args[0]
        elif len(args) == 2:
            self.ref = datetime.date(args[1], args[0], 1)

    def annee(self):
        return self.ref.year

    def numero(self):
        return self.ref.month

    def __str__(self):
        return "%d/%d" % (self.numero(), self.annee())

    def __repr__(self):
        return "Mois(%d, %d)" % (self.numero(), self.annee())

    def nom_long(self):
        return format_date(self.ref, "MMMM", 'fr_FR')

    def nom_court(self):
        return format_date(self.ref, "MMM", 'fr_FR')

    def nom_systeme(self):
        return lower_ascii(self.nom_court().replace('.', ''))

    def suivant(self):
        mois_suivant = self.numero() + 1
        if mois_suivant > 12:
            return Mois(1, self.annee() + 1)
        else:
            return Mois(mois_suivant, self.annee())

    def precedent(self):
        mois_precedent = self.numero() - 1
        if mois_precedent < 1:
            return Mois(12, self.annee() - 1)
        else:
            return Mois(mois_precedent, self.annee())

    def __add__(self, other):
        if other == 0:
            return self
        elif other < 0:
            return self.precedent().__add__(other + 1)
        else:
            return self.suivant().__add__(other - 1)

    def __sub__(self, other):
        return self.__add__(-other)

    def debut(self):
        if self.numero() == 1:
            jour = 2
        else:
            jour = 1
        return datetime.date(self.annee(), self.numero(), jour)

    def fin(self):
        if self.numero() == 12:
            recul = 2
        else:
            recul = 1
        suivant = self.suivant()
        return datetime.date(suivant.annee(), suivant.numero(), 1) - datetime.timedelta(days=recul)

    def __eq__(self, other):
        return self.numero() == other.numero() and self.annee() == other.annee()

    def __neq__(self, other):
        return not self.__eq__(other)

    @staticmethod
    def nom_vers_numero(nom):
        nom = lower_ascii(nom)
        for n in range(1, 12 + 1):
            candidat = Mois(n, 2000)
            if nom.startswith(candidat.nom_systeme()) and lower_ascii(candidat.nom_long()).startswith(nom):
                return n
        raise ValueError("%s n'est pas un mois connu" % nom)

    @staticmethod
    def nom_vers_mois(nom):
        num = Mois.nom_vers_numero(nom)
        courant = Mois()
        candidats = [courant + i for i in range(-8, 2 + 1)]
        assert len(candidats) <= 11
        for cand in candidats:
            if cand.numero() == num:
                return cand
        raise Ambiguous("Ambigu : %s passé ou %s prochain ?" % (nom, nom))
