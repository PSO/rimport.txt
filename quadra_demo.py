from backends.quadra import LigneQuadra
from compta import Ligne, Ecriture


class OptionProxy(object):
    @property
    def date(self):
        return datetime.date(2020, 9, 13)

    @property
    def compte(self):
        return '75620000'

    @property
    def titre(self):
        return "Test caracteres"


if __name__ == '__main__':
    import datetime
    options = OptionProxy()
    ecriture = Ecriture(options=options)
    ligne = Ligne(options.date, compte='9SOTI', montant=1, titre=b"200+ \200\201\202\203\204\205\206\207\x88\x89")
    ecriture.add_line(ligne)
    print(repr(LigneQuadra(ligne).data()))
    ligne = Ligne(options.date, compte='9SOTI', montant=1, titre=b"x88+ \x88\x89\x8A\x8B\x8C\x8D\x8E\x8F")
    ecriture.add_line(ligne)
    print(repr(LigneQuadra(ligne).data()))
    ecriture.add_line(Ligne(options.date, compte='9SOTI', montant=1, titre=b"x90+ \x90\x91\x92\x92\x94\x95\x96\x97\x98\x99\x9A\x9B\x9C\x9D\x9E\x9F"))
    with open("qgen.txt", "wb") as f:
        ecriture.dump_quadra(f)
