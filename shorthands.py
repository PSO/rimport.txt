"""
Raccourcis:
--cantine   --compte=706001 --date=%F       --titre="Cantine %m"
--sortie    --compte=706002
--clae      --compte=706003 --date=%F       --titre="CLAÉ %m"
--alsh      --compte=706004 --date=%F       --titre="Mercredis %m"
--adhesion  --compte=706110 --date=02/09/%A --titre="Adhésion à l'asso. %A"
--part      --compte=706120 --date=%D       --titre="Participation famille %m"
--exn       --compte=706120 --date=%D       --titre="Part. except. CLAE %m"
--papier    --compte=706140                 --titre="Forfait papier"
--dpt       --compte=706191                 --titre="Soutien Fédé. départ."
--regio     --compte=706192                 --titre="Soutien Fédé. régionale"
--cor       --compte=706193                 --titre="Adhésion Cor d'Oc"
--auto      --compte=707001
"""

import re

shorthand_pattern = re.compile('(--\\w+)(.*)')
shorthand_dict = {}

for line in __doc__.splitlines():
    m = shorthand_pattern.fullmatch(line)
    if m:
        repl = m.group(2).split('--')
        repl = [args.strip() for args in repl]
        repl = ['--' + args.replace('"', '') for args in repl if args]
        shorthand_dict[m.group(1)] = repl


def protect(doc_to_protect):
    return doc_to_protect.replace(' --', ' -\u007F-')


doc = protect(__doc__)


def expand(argv):
    res = []
    for arg in argv:
        new_args = shorthand_dict.get(arg, [arg])
        res += new_args
    return res
